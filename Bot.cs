﻿using System;
using Telegram.Bot;
using System.Threading;
using MihaZupan;
using System.Net;
using System.IO;
using System.Threading.Tasks;

namespace DerivativeBot
{
    public class Bot
    {
        private readonly ITelegramBotClient bot;

        public Bot()
        {
        
            var proxy = new HttpToSocks5Proxy("185.36.191.39", 5176, "userid37VL", "4Kweo2")
            {
                ResolveHostnamesLocally = true
            };

            bot = new TelegramBotClient("722985038:AAE2tsgvOEhVs7A4ADMNzsqXaguyHLtqVh0", proxy);
            bot.OnMessage += ActOnMessage;

        }

        public void Run()
        {
            Console.WriteLine("Starting...");
            bot.StartReceiving();
            Thread.Sleep(int.MaxValue);
        }
        
        private async void ActOnMessage(object sender, Telegram.Bot.Args.MessageEventArgs e)
        {
            if (e.Message.Text != null)
            {
                Console.WriteLine($"From {e.Message.Chat.Id}: {e.Message.Text}");
                string message;
                
                try { message = await GetDerivativeFromService(e.Message.Text); }
                catch { message = "Wrong input function"; }

                await bot.SendTextMessageAsync(chatId: e.Message.Chat, text: message);
            }
        }

        private async Task<string> GetDerivativeFromService(string expression)
        {
            var url = $"http://localhost:8080/?Expression={expression}";
            HttpWebResponse response = null;
            try
            {
                var request = WebRequest.Create(url) as HttpWebRequest;
                response = await request.GetResponseAsync() as HttpWebResponse;
            }
            catch { return "Service is unavailable"; }

            using (var stream = response.GetResponseStream())
            {
                using (var reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }    
        }
    }
}