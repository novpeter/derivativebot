﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.CSharp;

namespace Listener
{
    public class Parser
    {
        public static Expression<Func<double, double>> ConvertStringToExpression(string str)
        {
            var providerOptions = new Dictionary<string, string>
            {
                { "CompilerVersion", "v3.5" }
            };
            var provider = new CSharpCodeProvider(providerOptions);

            var results = provider.CompileAssemblyFromSource
            (
                    new CompilerParameters(new[] { "System.Core.dll" }),
@"using System;
using System.Linq.Expressions;

class Converter
{
    public static Expression<Func<double, double>> Convert()
    {
        return x =>" + str + @";
    }
}"
            );

            return (Expression<Func<double, double>>)results.CompiledAssembly
                .GetType("Converter")
                .GetMethod("Convert")
                .Invoke(null, null);
        }
    }
}
