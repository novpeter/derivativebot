﻿// Program.cs
using System;

namespace Listener
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            new Listener("http://localhost", "8080").StartListen();
        }
    }
}
