﻿using System;
using System.Linq.Expressions;

namespace Listener
{
   public static class Algebra
   {
      public static Expression<Func<double, double>> Differentiate(Expression<Func<double, double>> function)
         => Expression.Lambda<Func<double, double>>(DifferentiateBody(function.Body), function.Parameters);

      private static Expression DifferentiateBody(Expression body)
      {
         if (body is ConstantExpression) 
            return Expression.Constant(0.0);
         if (body is ParameterExpression)
            return Expression.Constant(1.0);
         if (body is BinaryExpression expression) 
            return HandleBinaryExpression(expression, body);
         if (body is MethodCallExpression methodCall) 
            return HandleMethodCall(methodCall, body);
         throw new ArgumentException();
      }

      private static Expression HandleBinaryExpression(BinaryExpression expression, Expression body)
      {
         var valueLeft = expression.Left;
         var valueRight = expression.Right;
                
         if (body.NodeType == ExpressionType.Add)
            return Expression.Add(DifferentiateBody(valueLeft), DifferentiateBody(valueRight));

         if (body.NodeType == ExpressionType.Multiply)
            return Expression.Add(Expression.Multiply(DifferentiateBody(valueLeft), valueRight), 
               Expression.Multiply(DifferentiateBody(valueRight), valueLeft));
         
         throw new ArgumentException("Wrong arguments");
      }
      
      private static Expression HandleMethodCall(MethodCallExpression methodCallExpression, Expression body)
      {
         var argument = methodCallExpression.Arguments[0];
         var sin = typeof(Math).GetMethod("Sin");
         var cos = typeof(Math).GetMethod("Cos");

         if (methodCallExpression.Method == sin)
         {
            if (argument is BinaryExpression)
               return Expression.MakeBinary(
                  ExpressionType.Multiply,
                  Expression.Call(cos,argument),
                  DifferentiateBody(argument)
               );
            return Expression.Call(cos, argument);
         }
         
         if (methodCallExpression.Method == cos)
         {
            if (argument is BinaryExpression)
               return Expression.MakeBinary(
                  ExpressionType.Multiply,
                  Expression.Negate(Expression.Call(sin,argument)),
                  DifferentiateBody(argument)
               );
            return Expression.Negate(Expression.Call(sin, argument));
         }
         
         throw new Exception("Wrong method call");
      }
   }
}