﻿// Listener.cs

using System;
using System.Net;
using System.Text;

namespace Listener
{
    public class Listener
    {

        public string Url { get; }
        public string Port { get; }
        public string Prefix => $"{Url}:{Port}/";
        public readonly HttpListener listener;

        public Listener(string url, string port)
        {
            Url = url;
            Port = port;
            
            listener = new HttpListener();
            listener.Prefixes.Add(Prefix);
            listener.Start();
        }
        
        
        public void StartListen()
        {
            Console.WriteLine($"Listening on {Prefix}...");
            while (true)
            {
                try
                {
                    var context = listener.GetContext();
                    var result = GetDerivative(context.Request.QueryString["Expression"]);
                    var buffer = Encoding.UTF8.GetBytes(result);
                    using (var outputStream = context.Response.OutputStream)
                    {
                        outputStream.Write(buffer, 0, buffer.Length);
                    }
                }
                catch (Exception e) { Console.WriteLine(e); }
            }

        }

        private string GetDerivative(string expression)
        {
            return Algebra.Differentiate(Parser.ConvertStringToExpression(expression)).ToString();
        }
    }
}
